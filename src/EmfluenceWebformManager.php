<?php

namespace Drupal\emfluence_webform;

use Drupal\Core\Config\ConfigFactoryInterface;

  /*
   * Get prepared list of supported webform fields.
   *
   * @return array
   *   Returns a list of supported webform fields.
   */
class EmfluenceWebformManager {

  public function get_fields() {
    // @codingStandardsIgnoreStart
    /*
     * tested not working & why
     *  
     * checkboxes - produces an array of elements
     * fieldset - I know... I'm working on it
     * address - a whole world of arrays that break all sorts of things
     * contact - see address
     * webform_likert - produces an array of elements
     * text_format - creates an array of content & format type that emflunce doesnt understand
     * webform_signature - can create too large a field
     * tableselect - can produce an array like checkboxes
     */
    // @codingStandardsIgnoreEnd

    $supported_fields = [
      'checkbox',
      'date',
      'datelist',
      'datetime',
      'email',
      'hidden',
      'number',
      'radios',
      'range',
      'select',
      'textarea',
      'tel',
      'tableselect',
      'textfield',
      'url',
      'value',
      'webform_autocomplete',
      'webform_codemirror',
      'webform_radios_other',
      'webform_rating',
      'webform_select_other',
      'webform_terms_of_service',
      'webform_time',
    ];

    return $supported_fields;

  }

  /*
   * Get prepared list of supported webform fields.
   *
   * @param $webform
   *   The ID of the webform.
   *
   * @param $message
   *   The message to be sent by email.
   *
   * @return bool
   *   Returns success by default.
   */
  public function sendMail($webform, $message) {

    $headers = "Content-Type: text/html; charset=\"UTF-8\"; format=flowed \r\n";
    $config_settings = \Drupal::config('emfluence_webform.settings');
    $send_to = $config_settings->get('debug_email');
    $subject = 'emfluence module debug for form ' . $webform . '.';

    mail($send_to, $subject, $message, $headers);

    return true;

  }

}