<?php

namespace Drupal\emfluence_webform\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation;

/**
 * Class emfluenceWebformFieldsMapping
 *
 * @package Drupal\emfluence_webform\Form
 */

class EmfluenceWebformFieldsMapping extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    // does this need to be changed back to webform_field_mapping or do webform_field_mapping references need to be updated?
    return 'emfluence_webform_fields_mapping';
  }

  /**
   * {@inheritdoc}
   */  
  public function buildForm(array $form, FormStateInterface $form_state) {

    $webform = \Drupal::entityTypeManager()->getStorage('webform')
      ->load(\Drupal::request()->get('webform'));
    $elements = Yaml::decode($webform->get('elements'));

    $form_state->setStorage(['webformId' => $webform->id(), 'elements' => $elements]);

    $default_values = $this->config('emfluence_webform.webform_field_mapping.' . $webform->id())->getRawData();

    $custom_form_values = $this->config('emfluence_webform.webform_form_control.' . $webform->id());
    $emfluence_global_values = $this->config('emfluence_webform.settings');

    // get list of supported fields
    $emfluence_manager = \Drupal::service('emfluence_webform.emfluence_manager');
    $tested_fields = $emfluence_manager->get_fields();

    $form['attach_form'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send this form to emfluence.'),
      '#default_value' => $custom_form_values->get('attach_form'),
      '#description' => $this->t('Select to send form submissions to emfluence.'),
      '#required' => FALSE,
      '#weight' => -20,
    ];

    $form['by_form_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Webform Settings'),
      '#states' => [
        'visible' => [
          ':input[name="attach_form"]' => ['checked' => TRUE]
        ],
      ],
    ];

    $form['by_form_settings']['group_ids'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Group IDs'),
      '#default_value' => $custom_form_values->get('group_ids'),
      '#description' => $this->t('Group IDs comma delinated.'),
      '#required' => FALSE,
      '#weight' => -20,
    ];

    $form['by_form_settings']['get_ip'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Capture IP'),
      '#default_value' => $custom_form_values->get('get_ip'),
      '#description' => $this->t('Include the IP address in data sent to emfluence (ipaddress)'),
      '#required' => FALSE,
      '#weight' => -18,
    ];

    $form['by_form_settings']['field-mapping'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Field Mappings'),
      '#weight' => 0,
    ];

    $form['by_form_settings']['field-mapping']['webform_container'] = [
      '#prefix' => "<div id=form-ajax-wrapper>",
      '#suffix' => "</div>",
    ];

    // Set counter to check if there are any supported fields and if not then display a message.
    $n = 0;

    foreach ($elements as $key => $element) {
      $selected_field = '_none';
      $custom_number = 1;

      // get the type of the field
      $type = $element['#type'];

      // Get the multiple value & if it is TRUE then the field is set to allow multiple values.
      $multiple = $element['#multiple'];

      // if we support the field then build the form entry
      if (in_array($type, $tested_fields) && !$multiple) {
  
        $n++;

        if (!empty($default_values[$key])) {
          $selected_field = $default_values[$key]['emfluence_field'];
          $custom_number = $default_values[$key]['custom_number'];
        }
		
        $form['by_form_settings']['field-mapping']['webform_container'][$key] = [
          '#type' => 'fieldset',
          '#title' => isset($element['#title']) ? $element['#title'] : $element['#type'],
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
        ];

        $form['by_form_settings']['field-mapping']['webform_container'][$key][$key . '_emfluence_field'] = [
          '#type' => 'select',
          '#options' => $this->getEmfluenceFields(),
          '#default_value' => $selected_field,
          '#title' => t('Select Emfluence Data Element field'),
          '#attributes' => ['id' => $key . '_emfluence_field'],
        ];

        $form['by_form_settings']['field-mapping']['webform_container'][$key][$key . '_custom_number'] = [
          '#type' => 'number',
          '#default_value' => $custom_number,
          '#min' => 1,
          '#max' => 25,
          '#step' => 1,
          '#title' => $this->t('Custom Field Number'),
          '#states' => [
            'visible' => [
              'select[id="' . $key . '_emfluence_field"]' => ['value' => 'custom']
            ],
          ],
        ];

      }
    }

    if ($n === 0) {
      \Drupal::messenger()->addWarning('This form does not contain any supported fields. Your form must contain at least one supported field to send data to emfluence. See help documentation for a list of supported fields.');
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
    ];
		
    return $form;
  }

  /**
   * {@inheritdoc}
   */  	
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    $storage = $form_state->getStorage();

    $data = [];
    $customs = [];

    foreach ($storage['elements'] as $key => $element) {
      $data[$key] = array(
        'emfluence_field' => $values[$key . '_emfluence_field'],
        'custom_number' => $values[$key . '_custom_number'],
      );
    }

    $config = \Drupal::configFactory()->getEditable('emfluence_webform.webform_field_mapping.' . $storage['webformId']);
    $config->setData($data);

    $control = \Drupal::configFactory()->getEditable('emfluence_webform.webform_form_control.' . $storage['webformId']);
    $control->set('attach_form', $values['attach_form'])
      ->set('group_ids', $values['group_ids'])
      ->set('get_ip', $values['get_ip']);
    $config->save(TRUE);
    $control->save(TRUE);

    \Drupal::messenger()->addMessage(t('Field mappings have been saved.'));

  }

  /**
   * Get prepared list of module fields.
   *
   * @return array
   *   Returns a list of module fields.
   */
  private function getEmfluenceFields() {

    $fields = array(
      '_none' => 'None',
      'firstName' => 'firstName',
      'lastName' => 'lastName',
      'email' => 'email',
      'company' => 'company',
      'title' => 'title',
      'phone' => 'phone',
      'fax' => 'fax',
      'address1' => 'address1',
      'address2' => 'address2',
      'city' => 'city',
      'state' => 'state',
      'zipCode' => 'zipCode',
      'country' => 'country',
      'dateofbirth' => 'dateofbirth',
      'memo' => 'memo',
      'custom' => 'custom',
      'originalSource' => 'originalSource',
      'suppressed' => 'suppressed',
    );

    return $fields;

  }	
	
}
