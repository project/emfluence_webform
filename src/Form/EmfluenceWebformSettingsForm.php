<?php

namespace Drupal\emfluence_webform\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\Entity\DateFormat;

/**
 * Class SherpaWebformSettingsForm
 *
 * @package Drupal\sherpa_webform\Form
 */
class EmfluenceWebformSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'emfluence_webform_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['emfluence_webform.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Identify if curl or restful services are available & construct a message on what is and is not available to the module
    $moduleHandler = \Drupal::service('module_handler');
    $message = '<ul><li>';

    // Assume that neither curl nor restful services are available
    $drupal_rest = FALSE;
    $curl = FALSE;
    if ($moduleHandler->moduleExists('rest')){
      $message .= 'The RESTful Web Services module is enabled. You can use RESTful Web Services.<br />';
      $drupal_rest = TRUE;
    } else {
      $message .= 'The RESTful Web Services module is disabled.<br />';
    }

    $message .= '</li><li>';

    if (function_exists('curl_version')) {
      $message .= 'cURL has been detected.<br />';
      $curl = TRUE;
    } else {
      $message .= 'cURL has not been detected.<br />';
    }

    $message .= '</li></ul>';

    if (!$drupal_rest && !$curl) {
      $message .= '<p><strong>Emfluence Webform cannot detect a method to send data to emfluence.</strong><br />';
      $message .= '<span style="color: #ff0000">You need either the RESTful Web Services module enabled or cURL installed and enabled on your server.</span></p>';
    } elseif ($drupal_rest && !$curl) {
      $message .= '<p><span style="color: #ff0000">Make sure you have the Use RESTful Web Services checkbox checked, below.</span></p>';
    }		

    $emfluence_webform_config = $this->config('emfluence_webform.settings');

    $form['emfluence_auth_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Authorization Key'),
      '#default_value' => $emfluence_webform_config->get('emfluence_auth_id'),
      '#size' => 64,
      '#maxlength' => 64,
      '#description' => $this->t('The Emfluence authorization key. This is provided by Emfluence to connect to the remote API.'),
      '#required' => TRUE,
    ];

    $form['emfluence_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Emfluence Remote URL'),
      '#default_value' => $emfluence_webform_config->get('emfluence_url'),
      '#size' => 64,
      '#maxlength' => 128,
      '#description' => $this->t('The URL of the remote emfluence server, including http or https.'),
      '#required' => TRUE,
    ];

    $form['enable_debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Debugging'),
      '#default_value' => $emfluence_webform_config->get('enable_debug'),
      '#description' => $this->t('Enable debugging. If disabled, only the POST / Response will be sent to the Drupal log.'),
      '#required' => FALSE,
    ];

    $form['debugging'] = [
      '#type' => 'fieldset',
      '#title' => 'Debugging Options',
      '#states' => [
        'visible' => [
          ':input[name="enable_debug"]' => ['checked' => TRUE]
        ],
      ],
    ];

    $tmp_send_debug_email = $emfluence_webform_config->get('send_debug_email');
    $form['debugging']['send_debug_email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send Debug Email'),
      '#default_value' => (isset($tmp_send_debug_email) ? $emfluence_webform_config->get('send_debug_email') : 0),
      '#description' => $this->t('Send an email that contains the raw JSON string along with the response object from emfluence. Useful sometimes as the log files do not properly display complex JSON strings.'),
      '#required' => FALSE,
    ];

    $tmp_debug_email = $emfluence_webform_config->get('debug_email');
    $form['debugging']['debug_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Debug Email'),
      '#default_value' => (isset($tmp_debug_email) ? $emfluence_webform_config->get('debug_email') : ''),
      '#states' => [
        'disabled' => [
          ':input[name="send_debug_email"]' => ['checked' => FALSE]
        ],
        'required' => [
          ':input[name="send_debug_email"]' => ['checked' => TRUE]
        ],
      ],
    ];

    $form['integration'] = [
      '#type' => 'fieldset',
      '#title' => 'Integration Options',
    ];

    $form['integration']['spit_it_out'] = [
      '#markup' => $message,
    ];

    $form['integration']['use_drupal'] = [
      '#type' => 'checkbox',
      '#title'=> $this->t('Use RESTful Web Services'),
      '#default_value' => (!$drupal_rest ? 0 : $emfluence_webform_config->get('use_drupal')),
      '#description' => $this->t('Use Drupal\'s RESTful Web Services module.') . (!$drupal_rest ? $this->t(' Module not enabled') : ''),
      '#required' => FALSE,
      '#disabled' => ($drupal_rest ? FALSE : TRUE),
    ];

    return parent::buildForm($form, $form_state);
	
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */  
  public function submitForm(array &$form, FormStateInterface $form_state) {

    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();

    $this->config('emfluence_webform.settings')
      ->set('emfluence_auth_id', $values['emfluence_auth_id'])
      ->set('emfluence_url', $values['emfluence_url'])
      ->set('enable_debug', $values['enable_debug'])
      ->set('send_debug_email', $values['send_debug_email'])
      ->set('debug_email', $values['debug_email'])
      ->set('use_drupal', $values['use_drupal'])
      ->save();

  }

}