# Emfluence Webform

To do...


## Version Information

0.2-beta:
- debug email conditional in module file
- update help document
- removed version from info to prep for promotion

0.1-beta:
- filter out "multiple fields" from field mappings
- add ability to capture client ip as part of emfluence submission
- debug curl detection
- cleaned up code to standard

0.9.1-alpha:
- removed field restriction from EmfluenceWebformFieldsMapping submitForm()

0.9-alpha:
- added sending debug emails with associated configuration
- expanded help docs to cover module configuration
- added integration options messages

0.8-alpha:
- add alert if no supported fields are available to map on field mapping form
- update mapping form id

0.7.1-alpha:
- quick fix of info file (correct version)

0.7-alpha:
- added new management class & services routing
- added list of tested webform fields & restrict field mapping to tested fields
- started work on supporting fieldset webform fields (and their children)
- started working on help documentation

0.6-alpha:
- tested emfluence custom fields and fixed them so they work

0.5-alpha:
- added custom fields in field mapping that appear to be properilly configured in the json but not tested in emfluence

0.4-alpha
- added sending using drupal restful web services module along with related configuration screen options

0.3-alpha
- Fixed mapping form issues
- Added version info to info file
- Fixed fat finger bug on submitForm in EmfluenceWebformSettingsForm 

0.2-alpha
- Fixed routing and installation bugs. Still untested in sending to emfluence (or submitting a form for that matter).

0.1-alpha:
- Built basic structure and code *UNTESTED*